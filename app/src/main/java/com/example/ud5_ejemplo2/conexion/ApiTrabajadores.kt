package com.example.ud5_ejemplo2.conexion

import com.example.ud5_ejemplo2.modelo.Trabajador
import retrofit2.Call
import retrofit2.http.*

interface ApiTrabajadores {
    // Obtener todos los trabajadores
    @GET("trabajadores")
    fun obtenerTrabajadores(): Call<ArrayList<Trabajador>>

    // Añadir nuevos trabajadores. El id es autoincremental.
    // Con la anotación "FormUrlEncoded" los valores de la url son codificados en tuplas clave/valor
    // separadas por '&' y con un '='  entre la clave y el valor.
    @FormUrlEncoded
    @POST("trabajadores")
    fun guardaTrabajador(@Field("nombre") nombre: String,
                         @Field("apellido") apellido: String,
                         @Field("email") email: String): Call<Trabajador>

    // Actualizar el trabajador asociado al id indicado.
    @PUT("trabajadores/{id}")
    fun actualizaTrabajador(@Path("id") id: Int, @Body trabajador: Trabajador): Call<Trabajador>

    // Borrar el trabajador asociado al id indicado.
    @DELETE("trabajadores/{id}")
    fun borraTrabajador(@Path("id") id: Int): Call<Trabajador>
}