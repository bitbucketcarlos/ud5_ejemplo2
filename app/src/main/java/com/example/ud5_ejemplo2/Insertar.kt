package com.example.ud5_ejemplo2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.example.ud5_ejemplo2.databinding.ActivityInsertarBinding

class Insertar : AppCompatActivity() {

    private lateinit var binding: ActivityInsertarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityInsertarBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        val botonInsertar = binding.insertarBoton
        val nombre = binding.nombreEdit
        val apellido = binding.apellidoEdit
        val email = binding.emailEdit

        botonInsertar.setOnClickListener {
            val intent = Intent().apply {
                putExtra("nombre", nombre.text.toString())
                putExtra("apellido", apellido.text.toString())
                putExtra("email", email.text.toString())
            }

            // Devolvemos un código de RESULT_OK
            setResult(RESULT_OK, intent)

            // Cerramos la actividad y volvemos a atrás.
            finish()
        }
    }
}