package com.example.ud5_ejemplo2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ud5_ejemplo2.conexion.ApiTrabajadores
import com.example.ud5_ejemplo2.conexion.Cliente
import com.example.ud5_ejemplo2.databinding.ActivityMainBinding
import com.example.ud5_ejemplo2.modelo.Trabajador
import com.google.android.material.snackbar.Snackbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    
    private var retrofit: Retrofit?= null
    private var trabajadorAdapter: TrabajadorAdapter ?=null
    private var posicionPulsada: Int = -1

    val resultInsertar = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val datosResult = result.data

            val trabajador = Trabajador(null, "", "", "")

            trabajador.nombre = datosResult?.getStringExtra("nombre").toString()
            trabajador.apellido = datosResult?.getStringExtra("apellido").toString()
            trabajador.email = datosResult?.getStringExtra("email").toString()

            insertarDatos(trabajador)
        }
    }

    val resultActualizarBorrar = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val datosResult = result.data

            // Borrar
            if(datosResult?.getBooleanExtra("Borrar", true) == true){
                borrarDatos(datosResult.getIntExtra("id", 0))
            }
            else{ // Actualizar
                val trabajador = Trabajador(datosResult?.getIntExtra("id", 0),
                                            datosResult?.getStringExtra("nombre").toString(),
                                            datosResult?.getStringExtra("apellido").toString(),
                                            datosResult?.getStringExtra("email").toString())

                actualizarDatos(trabajador)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Buscamos el RecyclerView e indicamos que su tamaño es fijo
        val recycler = binding.recyclerview
        val fab = binding.fab

        recycler.setHasFixedSize(true)

        // Añadimos la línea de separación de elementos de la lista
        // 0 para horizontal y 1 para vertical
        recycler.addItemDecoration(DividerItemDecoration(this, 1))

        // Asignamos un LinearLayout que contendrá cada elemento del RecyclerView
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        trabajadorAdapter = TrabajadorAdapter()

        // Asignamos el Listener
        trabajadorAdapter?.setOnItemClickListerner {
            posicionPulsada = recycler.getChildAdapterPosition(it)

            val trabajador = trabajadorAdapter?.getItem(posicionPulsada)

            val intent = Intent(this, ActualizarBorrar::class.java).apply {
                putExtra("id", trabajador?.id)
                putExtra("nombre", trabajador?.nombre)
                putExtra("apellido", trabajador?.apellido)
                putExtra("email", trabajador?.email)
            }

            resultActualizarBorrar.launch(intent)
        }

        // Asignamos el adapter al RecyclerView
        recycler.adapter = trabajadorAdapter

        fab.setOnClickListener {
            val intent = Intent(this, Insertar::class.java)

            resultInsertar.launch(intent)
        }

        retrofit = Cliente.obtenerCliente()

        obtenerDatos()
    }

    // Método para acceder obtener los datos a través de la API y añadirlos a la lista.
    private fun obtenerDatos() {
        // Hacemos uso de la ApiTrabajadores creada para obtener los valores pedidos.
        val api: ApiTrabajadores? = retrofit?.create(ApiTrabajadores::class.java)

        // Obtenemos la respuesta.

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        api?.obtenerTrabajadores()?.enqueue(object : Callback<ArrayList<Trabajador>> {
            override fun onResponse(call: Call<ArrayList<Trabajador>>, response: Response<ArrayList<Trabajador>>) {
                if (response.isSuccessful) {
                    val listaTrabajadores = response.body()

                    if (listaTrabajadores != null) {
                        trabajadorAdapter?.anyadirALista(listaTrabajadores)
                    }
                } else
                    Toast.makeText(applicationContext,"Fallo en la respuesta",Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<ArrayList<Trabajador>>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun insertarDatos(trabajador: Trabajador){
        // Hacemos uso de la ApiTrabajadores creada para obtener los valores pedidos.
        val api: ApiTrabajadores? = retrofit?.create(ApiTrabajadores::class.java)

        // Guardamos el trabajador

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        api?.guardaTrabajador(trabajador.nombre, trabajador.apellido, trabajador.email)?.enqueue(object : Callback<Trabajador> {
            override fun onResponse(call: Call<Trabajador>, response: Response<Trabajador>) {
                if (response.isSuccessful) {
                    val trab = response.body()

                    if (trab != null) {
                        trabajadorAdapter?.anyadirALista(trab)
                        Snackbar.make(binding.root, "Trabajador insertado correctamente", Snackbar.LENGTH_LONG)
                            .setAction("Aceptar"){
                            }
                            .show()
                    }
                } else
                    Toast.makeText(applicationContext,"Fallo en la respuesta", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<Trabajador>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun actualizarDatos(trabajador: Trabajador){
        // Hacemos uso de la ApiTrabajadores creada para obtener los valores pedidos.
        val api: ApiTrabajadores? = retrofit?.create(ApiTrabajadores::class.java)

        // Actualizamos el trabajador

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        // Utilizamos la operación let para confirmar que el id no es null
        trabajador.id?.let {
            api?.actualizaTrabajador(it, trabajador)?.enqueue(object : Callback<Trabajador> {
                override fun onResponse(call: Call<Trabajador>, response: Response<Trabajador>) {
                    if (response.isSuccessful) {
                        val trab = response.body()

                        if (trab != null) {
                            trabajadorAdapter?.actualizarLista(posicionPulsada, trab)
                            Snackbar.make(binding.root, "Trabajador actualizado correctamente", Snackbar.LENGTH_LONG)
                                .setAction("Aceptar"){
                                }
                                .show()
                        }
                    } else
                        Toast.makeText(applicationContext,"Fallo en la respuesta", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(call: Call<Trabajador>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    // Método para borrar un trabajador del servidor.
    private fun borrarDatos(id: Int){
        // Hacemos uso de la ApiTrabajadores creada para obtener los valores pedidos.
        val api: ApiTrabajadores? = retrofit?.create(ApiTrabajadores::class.java)

        // Borramos el trabajador

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        api?.borraTrabajador(id)?.enqueue(object : Callback<Trabajador> {
            override fun onResponse(call: Call<Trabajador>, response: Response<Trabajador>) {
                if (response.isSuccessful) {
                    val trab = response.body()

                    if (trab != null) {
                        trabajadorAdapter?.borrarDeLista(posicionPulsada)
                        Snackbar.make(binding.root, "Trabajador eliminado correctamente", Snackbar.LENGTH_LONG)
                            .setAction("Aceptar"){
                            }
                            .show()
                    }
                } else
                    Toast.makeText(applicationContext, "Fallo en la respuesta", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<Trabajador>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
            }
        })

    }
}