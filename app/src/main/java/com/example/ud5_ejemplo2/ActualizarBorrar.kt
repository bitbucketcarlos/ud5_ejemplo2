package com.example.ud5_ejemplo2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ud5_ejemplo2.databinding.ActivityActualizarBorrarBinding

class ActualizarBorrar : AppCompatActivity() {
    private lateinit var binding: ActivityActualizarBorrarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityActualizarBorrarBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        val idEdit = binding.idEditAct
        val nombre = binding.nombreEditAct
        val apellido = binding.apellidoEditAct
        val email = binding.emailEditAct
        val botonAct = binding.actualizarBoton
        val botonBor = binding.borrarBoton

        if(intent.hasExtra("id") && intent.hasExtra("nombre") && intent.hasExtra("apellido") && intent.hasExtra("email")) {
             // No podemos utilizar 'edit.text' ya que le tenemos que asignar un objeto Editable
            // Para asignarle el texto al EditText tenemos que utilizar el método setText.
            idEdit.setText(intent.getIntExtra("id", 0).toString())
            nombre.setText(intent.getStringExtra("nombre"))
            apellido.setText(intent.getStringExtra("apellido"))
            email.setText(intent.getStringExtra("email"))
        }

        botonAct.setOnClickListener {
            val intent = Intent().apply {
                putExtra("Borrar", false)
                putExtra("id", idEdit.text.toString().toInt())
                putExtra("nombre", nombre.text.toString())
                putExtra("apellido", apellido.text.toString())
                putExtra("email", email.text.toString())
            }

            // Devolvemos un código de RESULT_OK
            setResult(RESULT_OK, intent)

            // Cerramos la actividad y volvemos a atrás.
            finish()
        }

        botonBor.setOnClickListener {
            val intent = Intent().apply {
                putExtra("Borrar", true)
                putExtra("id", idEdit.text.toString().toInt())
            }

            // Devolvemos un código de RESULT_OK
            setResult(RESULT_OK, intent)

            // Cerramos la actividad y volvemos a atrás.
            finish()
        }

    }
}