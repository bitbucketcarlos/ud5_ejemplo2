package com.example.ud5_ejemplo2.modelo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/* Usamos la anotación "SerializedName" para indicar que queremos obtener el valor de la
   clave indicada, así podemos usar otro nombre para el atributo del POJO. En otro caso
   el atributo se debe llamar igual a la clave.
   La anotación "Expose" es para indicar que ese atributo debe ser expuesto para serializar o
   deserializarse en JSON. */
data class Trabajador (@SerializedName("id") @Expose var id: Int?,
                       @SerializedName("nombre") @Expose var nombre: String,
                       @SerializedName("apellido") @Expose var apellido: String,
                       @SerializedName("email") @Expose var email: String)
