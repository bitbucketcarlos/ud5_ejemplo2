package com.example.ud5_ejemplo2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ud5_ejemplo2.modelo.Trabajador

class TrabajadorAdapter: RecyclerView.Adapter<TrabajadorAdapter.MiViewHolder>() {

    private var lista: ArrayList<Trabajador> = ArrayList()
    private var listener:View.OnClickListener? = null

    // Creamos nuestro propio ViewHolder
    class MiViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idtextView: TextView
        val nombretextView: TextView
        val apellidotextView: TextView
        val emailtextView: TextView

        init {
            idtextView = view.findViewById(R.id.idtextView)
            nombretextView = view.findViewById(R.id.nombretextView)
            apellidotextView = view.findViewById(R.id.apellidotextView)
            emailtextView = view.findViewById(R.id.emailtextView)
        }
    }

    // Creamos nuevas views inflando el layout "elementos_lista"
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MiViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.elementos_lista, viewGroup, false)

        view.setOnClickListener(listener)

        return MiViewHolder(view)
    }

    // Establecemos el nombre y la aparición para el trabajador de esa posición
    override fun onBindViewHolder(viewHolder: MiViewHolder, position: Int) {
        viewHolder.idtextView.text = lista[position].id.toString()
        viewHolder.nombretextView.text = lista[position].nombre
        viewHolder.apellidotextView.text = lista[position].apellido
        viewHolder.emailtextView.text = lista[position].email
    }

    // Devolvemos el tamaño de la lista de trabajadores
    override fun getItemCount() = lista.size

    fun setOnItemClickListerner(onClickListener: View.OnClickListener){
        listener = onClickListener
    }

    // Devolvemos el elemento de la posición pos
    fun getItem(pos: Int) = lista[pos]

    // Método para añadir una lista de trabajadores al recyclerView. (Llamado para GET)
    fun anyadirALista(lista_: ArrayList<Trabajador>){
        lista.clear()
        lista.addAll(lista_)

        notifyDataSetChanged() // Actualizamos el recyclerView
    }

    // Método para añadir un trabajador al recyclerView. (Llamado para POST)
    fun anyadirALista(trabajador: Trabajador){
        lista.add(trabajador)

        notifyDataSetChanged() // Actualizamos el recyclerView
    }

    // Método para actualizar una posición del recyclerView. (Llamado para PUT)
    fun actualizarLista(pos: Int, trabajador: Trabajador){
        lista[pos] = trabajador

        notifyDataSetChanged() // Actualizamos el recyclerView
    }

    // Método para actualizar una posición del recyclerView. (Llamado para PUT)
    fun borrarDeLista(pos: Int){
        lista.removeAt(pos)

        notifyDataSetChanged() // Actualizamos el recyclerView
    }
}