# Ud5_Ejemplo2
_Ejemplo 2 de la Unidad 5._

Vamos a implementar una aplicación _CRUD_ que se conecte a nuestro servidor y permita crear, leer, modificar y eliminar trabajadores que tendrá los atributos id, nombre, apellido e email. 

Para ello crearemos nuestra propia API REST haciendo uso de un servidor _json-server_, que está implementado en _Node.js_ y permite trabajar con archivos _JSON_ para almacenar información.

Los pasos serán los siguientes:


## Paso 1: Modificar el fichero _build.gradle(:app)_
Añadiremos las siguientes dependencias para poder hacer uso de la librería _Retrofit_ y del convertidor de _JSON_ _GSON_
(Podemos ver la versión actual en [https://github.com/square/retrofit]):
```html
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
```

Además, para su correcto funcionamiento, deberemos comprobar que las siguientes líneas estas en el bloque _android_:
```html
android {
    ...
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    ...
}
```

## Paso 2: Dar permisos a la aplicación
Para poder realizar conexiones con servidores deberemos darle permisos a la aplicación. Para ello se debe añadir la siguiente línea en el fichero _AndrodManifest.xml_:
```html
<uses-permission android:name="android.permission.INTERNET"/>
```

## Paso 3: Creación de la interfaz _ApiTrabajadores_
Creamos una interfaz con los métodos HTTP a utilizar en nuestra aplicación. 
```java
interface ApiTrabajadores {
    // Obtener todos los trabajadores
    @GET("trabajadores")
    fun obtenerTrabajadores(): Call<ArrayList<Trabajador>>

    // Añadir nuevos trabajadores. El id es autoincremental.
    // Con la anotación "FormUrlEncoded" los valores de la url son codificados en tuplas clave/valor
    // separadas por '&' y con un '='  entre la clave y el valor.
    @FormUrlEncoded
    @POST("trabajadores")
    fun guardaTrabajador(@Field("nombre") nombre: String,
                         @Field("apellido") apellido: String,
                         @Field("email") email: String): Call<Trabajador>

    // Actualizar el trabajador asociado al id indicado.
    @PUT("trabajadores/{id}")
    fun actualizaTrabajador(@Path("id") id: Int, @Body trabajador: Trabajador): Call<Trabajador>

    // Borrar el trabajador asociado al id indicado.
    @DELETE("trabajadores/{id}")
    fun borraTrabajador(@Path("id") id: Int): Call<Trabajador>
}

```

## Paso 4: Creación de la clase _Cliente_
Creamos una clase en la que construiremos el objeto _Retrofit_. En la constante _URL_ deberemos asignar la dirección _IP_ de 
nuestro servidor (_10.0.2.2_ si estamos trabajando con _localhost_) y el puerto 3000 que es en el que trabaja.
```java
    companion object{
        const val URL:String = "http://10.0.2.2:3000"
        var retrofit: Retrofit?= null

        fun obtenerCliente(): Retrofit? {
            if(retrofit == null){
                // Construimos el objeto Retrofit asociando la URL del servidor y el convertidor Gson
                // para formatear la respuesta JSON.
                retrofit = Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            }

            return retrofit
        }
    }
}
```

## Paso 5: Creación de la clase _Trabajador_
Creamos un _POJO_ con los datos que queremos obtener del trabajador.
```java
data class Trabajador (@SerializedName("id") @Expose var id: Int?,
                       @SerializedName("nombre") @Expose var nombre: String,
                       @SerializedName("apellido") @Expose var apellido: String,
                       @SerializedName("email") @Expose var email: String)
```

## Paso 6: Creación de los _layouts_
Creamos los _layouts_ de la aplicación con el _RecyclerView_ y el _FAB_.

### _activity_main.xml_
```html
<androidx.constraintlayout.widget.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <com.google.android.material.floatingactionbutton.FloatingActionButton
        android:id="@+id/fab"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="end|bottom"
        android:src="@drawable/insertar"
        app:tint="@android:color/white"
        android:layout_marginEnd="32dp"
        android:layout_marginBottom="32dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```
### _elementos_lista.xml_
```html
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="wrap_content">

    <TextView
        android:id="@+id/idtextView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        tools:text="id"/>
    <TextView
        android:id="@+id/nombretextView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        tools:text="nombre"/>
    <TextView
        android:id="@+id/apellidotextView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        tools:text="apellido"/>
    <TextView
        android:id="@+id/emailtextView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        tools:text="email"/>

</LinearLayout>
```
### _activity_insertar.xml_
```html
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".Insertar">

    <TextView
        android:id="@+id/nombretextView"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Nombre:"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.207"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.341" />

    <EditText
        android:id="@+id/nombreEdit"
        android:layout_width="200dp"
        android:layout_height="wrap_content"
        android:hint="Nombre"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.53"
        app:layout_constraintStart_toEndOf="@+id/nombretextView"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.333" />

    <TextView
        android:id="@+id/apellidotextView"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Apellido:"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.207"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.425" />

    <EditText
        android:id="@+id/apellidoEdit"
        android:layout_width="200dp"
        android:layout_height="wrap_content"
        android:hint="Apellido"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.542"
        app:layout_constraintStart_toEndOf="@+id/apellidotextView"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.424" />

    <TextView
        android:id="@+id/emailtextView"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Email:"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.241"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <EditText
        android:id="@+id/emailEdit"
        android:layout_width="200dp"
        android:layout_height="wrap_content"
        android:hint="Email"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.542"
        app:layout_constraintStart_toEndOf="@+id/emailtextView"
        app:layout_constraintTop_toTopOf="parent" />

    <Button
        android:id="@+id/insertarBoton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Insertar"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/emailEdit" />

</androidx.constraintlayout.widget.ConstraintLayout>
```
### _activity_actualizar_borrar.xml_
```html
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".ActualizarBorrar">

    <TextView
        android:id="@+id/idAct"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Id:"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.285"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.273" />

    <EditText
        android:id="@+id/idEditAct"
        android:layout_width="200dp"
        android:layout_height="wrap_content"
        android:enabled="false"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.542"
        app:layout_constraintStart_toEndOf="@+id/idAct"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.268" />

    <TextView
        android:id="@+id/nombreAct"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Nombre:"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.207"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.341" />

    <EditText
        android:id="@+id/nombreEditAct"
        android:layout_width="200dp"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.53"
        app:layout_constraintStart_toEndOf="@+id/nombreAct"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.333" />

    <TextView
        android:id="@+id/apellidoAct"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Apellido:"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.207"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.425" />

    <EditText
        android:id="@+id/apellidoEditAct"
        android:layout_width="200dp"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.542"
        app:layout_constraintStart_toEndOf="@+id/apellidoAct"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.424" />

    <TextView
        android:id="@+id/emailAct"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Email:"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.241"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <EditText
        android:id="@+id/emailEditAct"
        android:layout_width="200dp"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.542"
        app:layout_constraintStart_toEndOf="@+id/emailAct"
        app:layout_constraintTop_toTopOf="parent" />

    <Button
        android:id="@+id/actualizarBoton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Actualizar"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.297"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/emailEditAct"
        app:layout_constraintVertical_bias="0.501" />

    <Button
        android:id="@+id/borrarBoton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Borrar"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.544"
        app:layout_constraintStart_toEndOf="@+id/actualizarBoton"
        app:layout_constraintTop_toBottomOf="@+id/emailEditAct"
        app:layout_constraintVertical_bias="0.501" />

</androidx.constraintlayout.widget.ConstraintLayout>
```
## Paso 7: Creación del adaptador _TrabajadorAdapter_
Creamos el adaptador para trabajar con el _RecyclerView_.

```java
class TrabajadorAdapter: RecyclerView.Adapter<TrabajadorAdapter.MiViewHolder>() {

    private var lista: ArrayList<Trabajador> = ArrayList()
    private var listener:View.OnClickListener? = null

    // Creamos nuestro propio ViewHolder
    class MiViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idtextView: TextView
        val nombretextView: TextView
        val apellidotextView: TextView
        val emailtextView: TextView

        init {
            idtextView = view.findViewById(R.id.idtextView)
            nombretextView = view.findViewById(R.id.nombretextView)
            apellidotextView = view.findViewById(R.id.apellidotextView)
            emailtextView = view.findViewById(R.id.emailtextView)
        }
    }

    // Creamos nuevas views inflando el layout "elementos_lista"
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MiViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.elementos_lista, viewGroup, false)

        view.setOnClickListener(listener)

        return MiViewHolder(view)
    }

    // Establecemos el nombre y la aparición para el trabajador de esa posición
    override fun onBindViewHolder(viewHolder: MiViewHolder, position: Int) {
        viewHolder.idtextView.text = lista[position].id.toString()
        viewHolder.nombretextView.text = lista[position].nombre
        viewHolder.apellidotextView.text = lista[position].apellido
        viewHolder.emailtextView.text = lista[position].email
    }

    // Devolvemos el tamaño de la lista de trabajadores
    override fun getItemCount() = lista.size

    fun setOnItemClickListerner(onClickListener: View.OnClickListener){
        listener = onClickListener
    }

    // Devolvemos el elemento de la posición pos
    fun getItem(pos: Int) = lista.get(pos)

    // Método para añadir una lista de trabajadores al recyclerView. (Llamado para GET)
    fun anyadirALista(lista_: ArrayList<Trabajador>){
        lista.clear()
        lista.addAll(lista_)

        notifyDataSetChanged() // Actualizamos el recyclerView
    }

    // Método para añadir un trabajador al recyclerView. (Llamado para POST)
    fun anyadirALista(trabajador: Trabajador){
        lista.add(trabajador)

        notifyDataSetChanged() // Actualizamos el recyclerView
    }

    // Método para actualizar una posición del recyclerView. (Llamado para PUT)
    fun actualizarLista(pos: Int, trabajador: Trabajador){
        lista.set(pos, trabajador)

        notifyDataSetChanged() // Actualizamos el recyclerView
    }

    // Método para actualizar una posición del recyclerView. (Llamado para PUT)
    fun borrarDeLista(pos: Int){
        lista.removeAt(pos)

        notifyDataSetChanged() // Actualizamos el recyclerView
    }
}
```
## Paso 8: Creación de las actividades
A continuación, creamos las actividades para insertar, actualizar y eliminar trabajadores.

### Insertar.kt
```java
class Insertar : AppCompatActivity() {

    private lateinit var binding: ActivityInsertarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityInsertarBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        val botonInsertar = binding.insertarBoton
        val nombre = binding.nombreEdit
        val apellido = binding.apellidoEdit
        val email = binding.emailEdit

        botonInsertar.setOnClickListener {
            val intent = Intent().apply {
                putExtra("nombre", nombre.text.toString())
                putExtra("apellido", apellido.text.toString())
                putExtra("email", email.text.toString())
            }

            // Devolvemos un código de RESULT_OK
            setResult(RESULT_OK, intent)

            // Cerramos la actividad y volvemos a atrás.
            finish()
        }
    }
}
```
### ActualizarBorrar.kt
```java
class ActualizarBorrar : AppCompatActivity() {
    private lateinit var binding: ActivityActualizarBorrarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityActualizarBorrarBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        val idEdit = binding.idEditAct
        val nombre = binding.nombreEditAct
        val apellido = binding.apellidoEditAct
        val email = binding.emailEditAct
        val botonAct = binding.actualizarBoton
        val botonBor = binding.borrarBoton

        if(intent.hasExtra("id") && intent.hasExtra("nombre") && intent.hasExtra("apellido") && intent.hasExtra("email")) {
             // No podemos utilizar 'edit.text' ya que le tenemos que asignar un objeto Editable
            // Para asignarle el texto al EditText tenemos que utilizar el método setText.
            idEdit.setText(intent.getIntExtra("id", 0).toString())
            nombre.setText(intent.getStringExtra("nombre"))
            apellido.setText(intent.getStringExtra("apellido"))
            email.setText(intent.getStringExtra("email"))
        }

        botonAct.setOnClickListener {
            val intent = Intent().apply {
                putExtra("Borrar", false)
                putExtra("id", idEdit.text.toString().toInt())
                putExtra("nombre", nombre.text.toString())
                putExtra("apellido", apellido.text.toString())
                putExtra("email", email.text.toString())
            }

            // Devolvemos un código de RESULT_OK
            setResult(RESULT_OK, intent)

            // Cerramos la actividad y volvemos a atrás.
            finish()
        }

        botonBor.setOnClickListener {
            val intent = Intent().apply {
                putExtra("Borrar", true)
                putExtra("id", idEdit.text.toString().toInt())
            }

            // Devolvemos un código de RESULT_OK
            setResult(RESULT_OK, intent)

            // Cerramos la actividad y volvemos a atrás.
            finish()
        }

    }
}
```

## Paso 9: Creación de la clase _MainActivity_
Por último, en la clase _MainActivity_ haremos uso de todas las clases creadas y podremos conectar con el servidor.
```java
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    
    private var retrofit: Retrofit?= null
    private var trabajadorAdapter: TrabajadorAdapter ?=null
    private var posicionPulsada: Int = -1

    val resultInsertar = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val datosResult = result.data

            val trabajador = Trabajador(null, "", "", "")

            trabajador.nombre = datosResult?.getStringExtra("nombre").toString()
            trabajador.apellido = datosResult?.getStringExtra("apellido").toString()
            trabajador.email = datosResult?.getStringExtra("email").toString()

            insertarDatos(trabajador)
        }
    }

    val resultActualizarBorrar = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val datosResult = result.data

            // Borrar
            if(datosResult?.getBooleanExtra("Borrar", true) == true){
                borrarDatos(datosResult.getIntExtra("id", 0))
            }
            else{ // Actualizar
                val trabajador = Trabajador(datosResult?.getIntExtra("id", 0),
                                            datosResult?.getStringExtra("nombre").toString(),
                                            datosResult?.getStringExtra("apellido").toString(),
                                            datosResult?.getStringExtra("email").toString())

                actualizarDatos(trabajador)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Buscamos el RecyclerView e indicamos que su tamaño es fijo
        val recycler = binding.recyclerview
        val fab = binding.fab

        recycler.setHasFixedSize(true)

        // Añadimos la línea de separación de elementos de la lista
        // 0 para horizontal y 1 para vertical
        recycler.addItemDecoration(DividerItemDecoration(this, 1))

        // Asignamos un LinearLayout que contendrá cada elemento del RecyclerView
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        trabajadorAdapter = TrabajadorAdapter()

        // Asignamos el Listener
        trabajadorAdapter?.setOnItemClickListerner {
            posicionPulsada = recycler.getChildAdapterPosition(it)

            val trabajador = trabajadorAdapter?.getItem(posicionPulsada)

            val intent = Intent(this, ActualizarBorrar::class.java).apply {
                putExtra("id", trabajador?.id)
                putExtra("nombre", trabajador?.nombre)
                putExtra("apellido", trabajador?.apellido)
                putExtra("email", trabajador?.email)
            }

            resultActualizarBorrar.launch(intent)
        }

        // Asignamos el adapter al RecyclerView
        recycler.adapter = trabajadorAdapter

        fab.setOnClickListener {
            val intent = Intent(this, Insertar::class.java)

            resultInsertar.launch(intent)
        }

        retrofit = Cliente.obtenerCliente()

        obtenerDatos()
    }

    // Método para acceder obtener los datos a través de la API y añadirlos a la lista.
    private fun obtenerDatos() {
        // Hacemos uso de la ApiTrabajadores creada para obtener los valores pedidos.
        val api: ApiTrabajadores? = retrofit?.create(ApiTrabajadores::class.java)

        // Obtenemos la respuesta.

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        api?.obtenerTrabajadores()?.enqueue(object : Callback<ArrayList<Trabajador>> {
            override fun onResponse(call: Call<ArrayList<Trabajador>>, response: Response<ArrayList<Trabajador>>) {
                if (response.isSuccessful) {
                    val listaTrabajadores = response.body()

                    if (listaTrabajadores != null) {
                        trabajadorAdapter?.anyadirALista(listaTrabajadores)
                    }
                } else
                    Toast.makeText(applicationContext,"Fallo en la respuesta",Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<ArrayList<Trabajador>>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun insertarDatos(trabajador: Trabajador){
        // Hacemos uso de la ApiTrabajadores creada para obtener los valores pedidos.
        val api: ApiTrabajadores? = retrofit?.create(ApiTrabajadores::class.java)

        // Guardamos el trabajador

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        api?.guardaTrabajador(trabajador.nombre, trabajador.apellido, trabajador.email)?.enqueue(object : Callback<Trabajador> {
            override fun onResponse(call: Call<Trabajador>, response: Response<Trabajador>) {
                if (response.isSuccessful) {
                    val trab = response.body()

                    if (trab != null) {
                        trabajadorAdapter?.anyadirALista(trab)
                        Snackbar.make(binding.root, "Trabajador insertado correctamente", Snackbar.LENGTH_LONG)
                            .setAction("Aceptar"){
                            }
                            .show()
                    }
                } else
                    Toast.makeText(applicationContext,"Fallo en la respuesta", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<Trabajador>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun actualizarDatos(trabajador: Trabajador){
        // Hacemos uso de la ApiTrabajadores creada para obtener los valores pedidos.
        val api: ApiTrabajadores? = retrofit?.create(ApiTrabajadores::class.java)

        // Actualizamos el trabajador

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        // Utilizamos la operación let para confirmar que el id no es null
        trabajador.id?.let {
            api?.actualizaTrabajador(it, trabajador)?.enqueue(object : Callback<Trabajador> {
                override fun onResponse(call: Call<Trabajador>, response: Response<Trabajador>) {
                    if (response.isSuccessful) {
                        val trab = response.body()

                        if (trab != null) {
                            trabajadorAdapter?.actualizarLista(posicionPulsada, trab)
                            Snackbar.make(binding.root, "Trabajador actualizado correctamente", Snackbar.LENGTH_LONG)
                                .setAction("Aceptar"){
                                }
                                .show()
                        }
                    } else
                        Toast.makeText(applicationContext,"Fallo en la respuesta", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure(call: Call<Trabajador>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    // Método para borrar un trabajador del servidor.
    private fun borrarDatos(id: Int){
        // Hacemos uso de la ApiTrabajadores creada para obtener los valores pedidos.
        val api: ApiTrabajadores? = retrofit?.create(ApiTrabajadores::class.java)

        // Borramos el trabajador

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        api?.borraTrabajador(id)?.enqueue(object : Callback<Trabajador> {
            override fun onResponse(call: Call<Trabajador>, response: Response<Trabajador>) {
                if (response.isSuccessful) {
                    val trab = response.body()

                    if (trab != null) {
                        trabajadorAdapter?.borrarDeLista(posicionPulsada)
                        Snackbar.make(binding.root, "Trabajador eliminado correctamente", Snackbar.LENGTH_LONG)
                            .setAction("Aceptar"){
                            }
                            .show()
                    }
                } else
                    Toast.makeText(applicationContext, "Fallo en la respuesta", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<Trabajador>, t: Throwable) {
                Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
            }
        })

    }
}
```